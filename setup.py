from distutils.core import setup

setup(name='PIK Batch Observer Framework and parallel tools',
      version='0.1',
      description='''
The Batch Observer Framework is a small framework for performing numerical experiments.
The simulation runs are grouped in batches, and the batches are processed by Observers locally before being aggregated across all runs.
''',
      author='Frank Hellmann',
      author_email='hellmann@pik-potsdam.de',
      packages=['baobap'], requires=['scipy', 'numpy', 'matplotlib', 'h5py']
      )
