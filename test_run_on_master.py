from baobap.mpi_tools import *

@run_on_master
def test1():
    print(("Inner on rank {}".format(rank)))
    return 5

@run_on_master
def test2():
    print(("Outer on rank {}".format(rank)))
    a = test1() + 6
    return a

print(("Result of function call on rank {} is {}".format(rank, test2())))