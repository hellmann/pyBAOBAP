# This example shows how to run a Monte Carlo experiment with pyBAOBAP. We will study the Lorenz dynamics.
# The study is structured into batches. Each batch consists of a number of runs. The results are then aggregated for
# each batch. As opposed to example 1 we will set up a full experimental infrastructure here, which is a bit overkill
# for this case but illustrates the use of some of the more common utility functions in pyBAOBAP.

import numpy as np
import baobap as bao
import os
import sys

# We will use the same dynamics as in example 1:
from lorenz_example import rhs, generate_run_conditions, lorenz_ob

default_dir = os.path.join("simulation_data", "lorenz")


def perform_experiment(times, sim_dir=default_dir):
    print("Running experiment")

    # This follows the same logic as example 1:
    result_file, batch_dir = bao.prep_dir(sim_dir)

    # Create a timestamp in the experiment directory:
    bao.timestamp_directory(sim_dir)

    brp = bao.BatchRunParameters(number_of_batches=10, simulations_per_batch=20, system_dimension=3)
    ob = bao.combine_obs(bao.run_condition_observer, lorenz_ob)
    bao.run_experiment(result_file, brp, None, generate_run_conditions, ob, times, figures=(True, 5), rhs=rhs)

    # We can now save the relevant experimental state for later analysis. This function takes the result_file and a
    # tuple to be saved for later:
    analysis_file = bao.save_state_for_analysis(result_file, (brp,))

    return analysis_file


# This decorator ensures that the function is only run on the master node and the results are distributed across
# processes correctly. It is useful to have a separate analyze step, so one can change and develop the analysis easily
# without rerunning the simulation.
@bao.run_on_master
def analyze(analysis_file):
    print("Analyzing data:\n\n")

    # We load the state we recorded at the end of the data generation to run analysis code:
    result_file, (brp,) = bao.load_state_for_analysis(analysis_file)

    res = bao.load_all_fields_from_results(result_file)

    print(f"The keys in the result file are: {res.keys()}")

    print(f"The fields rhs_arguments_1 contains the rho values used for the simulation:")
    print(res['rhs_arguments_1'])

    print(f"For each run, in each batch, and each system dimension we recorded the maximum value attained in the field "
          + f"max_values, with shape {res['max_values'].shape}")

    rhos = res['rhs_arguments_1'][:, 0]
    max_x0s = np.average(res['max_values'][:, :, 0], axis=1)

    print(f"We look at max_0^5(rho), The average maximum in x[0] for a given rho.")
    for rho, max_x0 in zip(rhos, max_x0s):
        print(f"max_0^5({rho}) = {max_x0}")


def experiment1(sim_dir=default_dir):
    print("Experiment 1")

    exp_dir = os.path.join(sim_dir, "experiment1")

    # Experiment 1 uses these times:
    times = np.linspace(0, 10, 1000)

    # If a experimental data doesn't exist, or a rerun has been specified, perform the full experiment
    if not os.path.exists(os.path.join(exp_dir, "analysis.p")) or 'rerun' in set(sys.argv):
        analysis_file = perform_experiment(times, exp_dir)
    else:
        analysis_file = os.path.join(exp_dir, "analysis.p")

    # Analyze the data:
    analyze(analysis_file)


def experiment2(sim_dir=default_dir):
    raise NotImplementedError("Experiment 2 has not been implemented yet")


if __name__ == "__main__":

    # By specifying separate experiments we can record the precise code that generated a particular dataset.
    # If a variant needs to be run later, it is simple to add a new experiment function.

    if 'exp1' in set(sys.argv):
        experiment1()

    if 'exp2' in set(sys.argv):
        experiment2()
