from baobap.bao_core import *


class LorenzPars(object):
    def __init__(self):
        self.system_size=3
        self.sigma = 10.
        self.rho = 28.
        self.beta = 8. / 3.


# I like to define right hand side functions as closures over the right hand side parameters,
# but this code might just as well be implemented in LorenzGen.__init__()

def gen_Lorenz_rhs(pars):
    assert isinstance(pars, LorenzPars)

    # y is an array of length 3
    # noinspection PyUnusedLocal
    def rhs(y, t):
        return np.array([pars.sigma * (y[1] - y[0]), y[0] * (pars.rho - y[2]) - y[0], y[0] * y[1] - pars.beta * y[2]])

    return rhs


class LorenzObs(Observer):
    def __init__(self, brp, batch_dir):
        super(LorenzObs, self).__init__(brp, batch_dir)
        self.max_values = np.zeros((brp.simulations_per_batch, brp.system_dimension))

    def update(self, time_series, run, initial_conditions):
        super(LorenzObs, self).update(time_series, run, initial_conditions)
        self.max_values[run] = np.max(time_series, axis=0)


def main():
    plotting_example = False
    framework_example = True

    if plotting_example:
        # using the heavy machinery to look at a single time_series...

        brp = BatchRunParameters(system_dimension=3)
        pars = LorenzPars()
        gen = RhsGen(brp, gen_Lorenz_rhs(pars))
        rcg = RCGenRand(brp, max_rand=[5, 10, 20])
        times = np.linspace(0, 10, 1000)
        time_series = gen.create_ts(rcg.gen(0, 0), times)

        import matplotlib.pyplot as plt

        plt.figure()
        plt.plot(times, time_series)

        plt.figure()
        plt.plot(time_series[:, 0], time_series[:, 1])

        plt.show()

    if framework_example:
        # run a full simulation run of the Lorenz system, 10 batches, 10 simulations in each batch, default parameters.

        np.random.seed(0)

        # Functions decorated with run_on_master will only be run on the master node, you need to broadcast the result
        # to the other nodes.
        @run_on_master
        def prep_dir(sim_dir):
            result_file_l = os.path.join("simulation_data", "results.hdf")
            batch_dir_l = os.path.join(sim_dir, "batch_data")
            if not os.path.exists(batch_dir_l):
                os.makedirs(batch_dir_l)
            return result_file_l, batch_dir_l

        # res = prep_dir("simulation_data")
        # res = comm.bcast(res, root=0)
        # result_file, batch_dir = res

        result_file, batch_dir = prep_dir("simulation_data")

        times = np.linspace(0, 10, 1000)

        brp = BatchRunParameters(number_of_batches=10, simulations_per_batch=10, system_dimension=3)
        rhs = gen_Lorenz_rhs(LorenzPars())
        rcg = RCGenRand(brp)
        gen = RhsGen(brp, rhs=rhs)
        obs = LorenzObs(brp, batch_dir)

        run_simulation_classes(result_file, brp, gen, rcg, obs, times)


if __name__ == "__main__":
    main()
