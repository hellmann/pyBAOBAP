

import numpy as np
import baobap as bao
from numba import njit
import os

default_dir = os.path.join("simulation_data", "lorenz")

# We use sigma_rho here to test that a tuple of an array and a scalar works correctly as parameters.
@njit
def rhs(y, t, sigma_rho, beta):
    return np.array([sigma_rho[0] * (y[1] - y[0]), y[0] * (sigma_rho[1] - y[2]) - y[0], y[0] * y[1] - beta * y[2]])


def generate_run_conditions(batch, run):
    ic = [5, 10, 20] * np.random.random(3)
    sigma, rho, beta = 10., 28., 8. / 3.
    return ic, (np.array([sigma, rho]), beta)


# This function observes the results of the simulation run and returns its observations in a dict
# noinspection PyUnusedLocal
def lorenz_ob(time_series, rc):
    return{"max_values": np.max(time_series, axis=0)}


def main(sim_dir=default_dir, create_test_data=True, run_test=True):
    np.random.seed(0)

    result_file, batch_dir = bao.prep_dir(sim_dir)

    times = np.linspace(0, 10, 1000)
    brp = bao.BatchRunParameters(number_of_batches=10, simulations_per_batch=10, system_dimension=3)
    ob = bao.combine_obs(bao.run_condition_observer, lorenz_ob)
    bao.run_experiment(result_file, brp, None, generate_run_conditions, ob, times, figures=(True, 5), rhs=rhs)

    res = bao.load_all_fields_from_results(result_file)

    if bao.am_master:

        if create_test_data:
            for key in list(res.keys()):
                np.save(os.path.join(sim_dir, key), res[key])

        if run_test:
            for key in list(res.keys()):
                filename = key + ".npy"
                with open(os.path.join(sim_dir, filename), mode='rb') as f:
                    arr = np.load(f)
                    assert np.allclose(arr, res[key])

if __name__ == "__main__":
    main()
