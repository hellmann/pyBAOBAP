

import numpy as np
import baobap as bao
from numba import njit, void, float64, int8
import os

# This example uses the ability to overwrite the create_ts function in the RhsGen in order to create a discrete
# timeseries of the logistic map instead of an odeint integrated one. THe times parameter takes on new meaning in this
# case and simply becomes a number indicating the length of the time series to be created. The observer takes the second
# half of the created time series and computes the histogram of that.

# We use a closure to define a run condition generator that varies r over batches and runs.

default_dir = os.path.join("simulation_data", "logistic_map")

# @njit(void(float64[:], int8, float64))
def logistic(states, length, r):
    for i in range(length - 1):
        states[i+1] = r * states[i] * (1. - states[i])


class LogTSGen(bao.TSGen):
    def create_ts(self, rc):
        ts = np.zeros(self.times, dtype=np.float64)
        ts[0] = rc[0]
        logistic(ts, self.times, rc[1])
        return ts


def define_gen_rc(brp, lower, upper):
    norm = 1. / (brp.number_of_batches * brp.simulations_per_batch)
    def generate_run_conditions(batch, run):
        ic = np.random.random()
        r = lower + (upper - lower) * (batch * brp.simulations_per_batch + run) * norm
        return ic, r
    return generate_run_conditions


# This function observes the results of the simulation run and returns its observations in a dict
# noinspection PyUnusedLocal
def log_ob(time_series, rc):
    length=len(time_series)
    hist, _ = np.histogram(time_series[length//2:], bins=100, range=(0.0, 1.0))
    return{"r": rc[1], "hist": hist}


def main(sim_dir=default_dir, create_test_data=False, run_test=False):
    np.random.seed(0)

    result_file, batch_dir = bao.prep_dir(sim_dir)

    times = 1000
    brp = bao.BatchRunParameters(number_of_batches=10, simulations_per_batch=100, system_dimension=1)
    ob = bao.combine_obs(log_ob, bao.obs_tail)
    gen_rc = define_gen_rc(brp, 2.0, 4.0)
    ts_gen = LogTSGen(brp, times=times)
    bao.run_experiment(result_file, brp, ts_gen, gen_rc, ob, figures=(True, 50))
    bao.save_experiment_script(result_file, __file__)

    res = bao.load_all_fields_from_results(result_file)

    if bao.am_master:

        if create_test_data:
            for key in list(res.keys()):
                np.save(os.path.join(sim_dir, key), res[key])

        if run_test:
            for key in list(res.keys()):
                filename = key + ".npy"
                with open(os.path.join(sim_dir, filename), mode='rb') as f:
                    arr = np.load(f)
                    assert np.allclose(arr, res[key])


@bao.run_on_master
def plot_histograms(sim_dir=default_dir):
    result_file = os.path.join(sim_dir, "results.hdf")
    brp = bao.BatchRunParameters(**bao.get_brp_dict(result_file))

    res = bao.load_all_fields_from_results(result_file)

    hist_block = res["hist"]
    tail_block = res["tail"]
    r_block = res["r"]

    hist = hist_block.reshape((hist_block.shape[0] * hist_block.shape[1], hist_block.shape[2]))
    r = r_block.reshape((r_block.shape[0] * r_block.shape[1],))
    tail = tail_block.reshape((tail_block.shape[0] * tail_block.shape[1], tail_block.shape[2]))

    import matplotlib.pyplot as plt

    # This figure relies on the fact that we chose to increase r in runs and batches in a way mirroring the numpy
    # reshape operation.
    plt.figure()
    plt.imshow(hist.T, cmap="Greys")
    plt.savefig(os.path.join(sim_dir, "log_histograms.pdf"))

    plt.figure()
    plt.plot(r, tail, '.', alpha=0.01, color='black')
    plt.savefig(os.path.join(sim_dir, "log_tail.png"))


if __name__ == "__main__":
    main()
    plot_histograms()