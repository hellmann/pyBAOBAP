# This example shows how to run a Monte Carlo experiment with pyBAOBAP. We will study the Lorenz dynamics.
# The study is structured into batches. Each batch consists of a number of runs. The results are then aggregated for
# each batch.

import numpy as np
import baobap as bao
from numba import njit
import os

default_dir = os.path.join("simulation_data", "lorenz")


# The right hand side function defines the system to study. It can contain parameters after the signature y, t. It will
# be integrated using scipy odeint.
@njit
def rhs(y, t, sigma, rho, beta):
    return np.array([sigma * (y[1] - y[0]), y[0] * (rho - y[2]) - y[0], y[0] * y[1] - beta * y[2]])


# The run condition generator returns the initial conditions and the parameter values to use for each run in each batch.
def generate_run_conditions(batch, run):
    # We choose random initial conditions...
    ic = [5, 10, 20] * np.random.random(3)
    # ... and vary the parameter sigma for each batch:
    sigma, rho, beta = 10., (batch + 2) * 28., 8. / 3.
    # We have to return an initial condition and a tuple of parameters.
    return ic, (sigma, rho, beta)

# The run condition generator has to fit the signature of the right hand side function:
if __name__ == "__main__":

    t = 0.
    ic, pars = generate_run_conditions(0, 0)
    r = rhs(ic, t, *pars)
    print(f"The right hand side evaluated at random initial conditions and parameters for batch 0 is {r}")

# PyBAOBAP generates a timeseries with the given dynamics and hands this timeseries to an observer function that records
# properties of it (or the entire timeseries). It returns these as a dictionary. The dictionaries are automatically
# combined and saved in hdf files. Several observer functions can be combined using the utility function combine_obs.

# noinspection PyUnusedLocal
def lorenz_ob(time_series, rc):
    # We record the maximum in each coordinate
    return{"max_values": np.max(time_series, axis=0)}

def simple_experiment(sim_dir=default_dir):
    print("Running simple experiment")

    # The convenience function prep_dir prepares the directory to run the experiment in:
    result_file, batch_dir = bao.prep_dir(os.path.join(sim_dir, "simple_experiment"))

    # Specify the times for which to simulate the dynamics:
    times = np.linspace(0, 10, 1000)

    # Set the parameters for the experiment:
    brp = bao.BatchRunParameters(number_of_batches=10, simulations_per_batch=20, system_dimension=3)

    # Define the observer by combining the predefined observer that records the run conditions with the observer we
    # defined above:
    ob = bao.combine_obs(bao.run_condition_observer, lorenz_ob)

    # Perform the experiment. This signature is specific to using a rhs and odeint internally. We also generate Figures
    # of a sample of the generated timeseries:
    bao.run_experiment(result_file, brp, None, generate_run_conditions, ob, times, figures=(True, 5), rhs=rhs)

    # The convenience function load_all_fields_from_results reads in the results of the experiment from the generated
    # hdf file:

    # Note that if you parallelize this, the code will run on each process in parallel. The bao functions are safe for
    # this usecase, but to run only on the master node we have to gate the rest of this code explicitly:

    if bao.am_master:
        print("Analyzing data:\n\n")

        res = bao.load_all_fields_from_results(result_file)

        print(f"The keys in the result file are: {res.keys()}")

        print(f"The fields rhs_arguments_1 contains the rho values used for the simulation:")
        print(res['rhs_arguments_1'])

        print(f"For each run, in each batch, and each system dimension we recorded the maximum value attained in the field "
              + f"max_values, with shape {res['max_values'].shape}")

        rhos = res['rhs_arguments_1'][:,0]
        max_x0s = np.average(res['max_values'][:,:,0], axis=1)

        print(f"We look at max_0^5(rho), The average maximum in x[0] for a given rho.")
        for rho, max_x0 in zip(rhos,max_x0s):
            print(f"max_0^5({rho}) = {max_x0}")

if __name__ == "__main__":
    simple_experiment()
