## Python 2 Legacy version

To access the Python 2 legacy version clone it from the branch last-python2-version, e.g.:

``` bash
git clone git@gitlab.pik-potsdam.de:hellmann/pyBAOBAP.git --branch last-python2-version --single-branch
```

# PyBAOBAP

This project provides a framework for running parallel numerical experiments. It uses MPI to parallelize the
computations.

The project contains a core experimental set up that runs experiments in parallel, grouped in batches. It further 
contains a number of helper functions that facilitate setting up an experiment in a parallel environment.

The standard way to use the framework for these pruposes is demonstrated in the lorenz examples.

lorenz_example.py contains code to study properties of the lorenz dynamics.

lorenz_example_2.py contains code for a systematic set up in which several experiments can be conveniently performed and
recorded.

Documentation and examples are a work in progress, and the API is liable to change in the future.