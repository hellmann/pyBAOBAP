
from .mpi_tools import *
from .hdfutils import load_dict_from_hdf5_group
import os
import pickle as p
import numpy as np
import h5py
__author__ = 'Frank Hellmann, hellmann@pik-potsdam.de'



@run_on_master
def ensure_dir_exists(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


# Here we set up the requred directories. This should only run on the master
@run_on_master
def prep_dir(sim_dir):
    result_file = os.path.join(sim_dir, "results.hdf")
    batch_dir = os.path.join(sim_dir, "batch_data")
    ensure_dir_exists(batch_dir)
    return result_file, batch_dir


@run_on_master
def timestamp_directory(directory):
    import time
    ts = "{0.tm_year}-{0.tm_mon}.{0.tm_mday}.-{0.tm_hour}h{0.tm_min}m.timestamp".format(time.localtime(time.time()))
    with open(os.path.join(directory, ts), mode='w') as f:
        f.write("timestamp")




@run_on_master
def save_state_for_analysis(result_file, *args):
    pickle_file = os.path.join(os.path.dirname(result_file), "analysis.p")
    with open(pickle_file, mode='wb') as f:
        p.dump((result_file,) + args, f)
    return pickle_file


def load_state_for_analysis(pickle_file):
    with open(pickle_file, mode='rb') as f:
        try:
            args = p.load(f)
        except UnicodeDecodeError:
            print("Python 2 pickle encoding fallback")
            args = p.load(f, encoding="latin1")

    return args


def get_brp_dict(result_file):
    with h5py.File(result_file, mode='r') as h5f:
        brp_dict = load_dict_from_hdf5_group(h5f["batch_run_parameters"])
    return brp_dict


def get_all_field_properties(result_file):
    with h5py.File(result_file, mode='r') as h5f:
        shapes = dict()
        dtypes = dict()
        for key in list(h5f["batch0"].keys()):
            if not isinstance(h5f["batch0"][key], h5py.Group):
                data = np.array(h5f["batch0"][key])
                shapes[key] = data.shape
                dtypes[key] = data.dtype
    return shapes, dtypes


def get_field_properties(result_file, field_name):
    with h5py.File(result_file, mode='r') as h5f:
        data = np.array(h5f["batch0"][field_name])
        shape = data.shape
        dtype = data.dtype
    return shape, dtype


def load_field_from_results(result_file, field_name, number_of_batches=None):
    shape, dtype = get_field_properties(result_file, field_name)
    if number_of_batches is None:
        brp_dict = get_brp_dict(result_file)
        number_of_batches = brp_dict["number_of_batches"]
    res = np.zeros((number_of_batches,) + shape, dtype=dtype)
    with h5py.File(result_file, mode='r') as h5f:
        for i in range(number_of_batches):
            res[i] = np.array(h5f["batch" + str(i)][field_name])

    return res


def load_all_fields_from_results(result_file, number_of_batches=None):
    if number_of_batches is None:
        brp_dict = get_brp_dict(result_file)
        number_of_batches = brp_dict["number_of_batches"]
    shapes, dtypes = get_all_field_properties(result_file)
    res = dict()
    for key in list(shapes.keys()):
        res[key] = np.zeros((number_of_batches,) + shapes[key], dtype=dtypes[key])
    with h5py.File(result_file, mode='r') as h5f:
        for i in range(number_of_batches):
            for key in list(shapes.keys()):
                res[key][i] = np.array(h5f["batch" + str(i)][key])

    return res

@run_on_master
def save_experiment_script(result_file, file_to_save):
    import shutil
    directory = os.path.dirname(result_file)
    shutil.copy(file_to_save, directory)