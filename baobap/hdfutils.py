__author__ = 'frank'

import numpy as np
import h5py


def save_class_to_hdf5_group(cla, hdf5_group):
    assert isinstance(hdf5_group, h5py.Group)
    for attr in vars(cla):
        hdf5_group[attr] = getattr(cla, attr)


def init_class_from_hdf5_group(hdf5_group, Cla, cla=None):
    assert isinstance(hdf5_group, h5py.Group)
    assert isinstance(cla, Cla) or cla is None
    if cla is None:
        cla = Cla()

    for attr in vars(cla):
        assert attr in list(hdf5_group.keys())
        # Need to cast this to numpy array explicitly as the fields are otherwise set to datasets
        data = np.array(hdf5_group[attr])
        # Then extract scalars and strings. I don't quite know why the code below works for strings but it seems to.
        if len(data.flatten()) == 1:
            data = data.flatten()[0]
        setattr(cla, attr, data)
        del data

    return cla


def save_dict_to_hdf5_group(di, hdf5_group):
    assert isinstance(hdf5_group, h5py.Group)
    for attr in list(di.keys()):
        hdf5_group[attr] = di[attr]


def load_dict_from_hdf5_group(hdf5_group, di=None):
    assert isinstance(hdf5_group, h5py.Group)
    if di is None:
        di = dict()

    for attr in list(hdf5_group.keys()):
        # ignore possible subgroups:
        if isinstance(hdf5_group[attr], h5py.Dataset):
            # Need to cast this to numpy array explicitly as the fields are otherwise set to datasets
            di[attr] = np.array(hdf5_group[attr])

    return di
