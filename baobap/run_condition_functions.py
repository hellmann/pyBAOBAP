
__author__ = 'Frank Hellmann, hellmann@pik-potsdam.de'
import numpy as np
from .bao_core import RCGen, BatchRunParameters
import os
from .hdfutils import *

def generate_single_node_rc_gen(node_idx, max_rand, fixpoint, *args):
    # node_idx is either an array of nodes to perturb or an array of one such arrays per batch
    if len(node_idx.shape) == 1:
        length = node_idx.shape[0]
        def rcg(batch, run):
            ic = np.copy(fixpoint)
            ic[node_idx] += max_rand * 2. * (np.random.random(length) - 0.5)
            return ic, args
    elif len(node_idx.shape) == 2:
        nnodes = node_idx.shape[0]
        length = node_idx.shape[1]
        def rcg(batch, run):
            ic = np.copy(fixpoint)
            ic[node_idx[batch % nnodes]] += max_rand * 2. * (np.random.random(length) - 0.5)
            return ic, args
    else:
        print("No valid list of nodes to disturb given (parameter node_idx has wrong shape).")
        return None

    return rcg


def generate_random_rc_gen(nodes, max_rand, fixpoint, *args):
    def rcg(batch, run):
        ic = np.copy(fixpoint)
        ic[nodes] += max_rand * 2. * (np.random.random(len(nodes)) - 0.5)
        return ic, args
    return rcg



class RCGenRand(RCGen):
    """ This class draws random initial conditions, optionally scaled to max_rand
    """

    def __init__(self, brp, max_rand=1, **kwargs):
        """ Optional argument max_rand, which can be a scalar or an array of length rhs_par.system_dimension. """
        super(RCGenRand, self).__init__(brp, **kwargs)
        self.max_rand = max_rand
        self.dim = brp.system_dimension

    def gen(self, batch, run):
        ic = self.max_rand * np.random.random(self.dim)
        return ic, tuple()


class RCGenFile(RCGen):
    """ This class reads initial conditions from a results file
    """

    def __init__(self, brp, h5file='', **kwargs):
        """ Optional argument max_rand, which can be a scalar or an array of length rhs_par.system_dimension. """
        super(RCGenFile, self).__init__(brp, **kwargs)
        self.file = h5file
        assert (os.path.exists(h5file))

        with h5py.File(h5file, mode='r') as h5f:
            self.brp = init_class_from_hdf5_group(h5f["batch_run_parameters"], BatchRunParameters)
            rc = np.array(h5f["batch0"]["run_conditions"])
            rc_shape = (self.brp.number_of_batches,) + rc.shape
            self.rcs = np.zeros(rc_shape, dtype=np.float64)

            for key in h5f.iter():
                if len(key) > 5 and key[:5] == "batch":
                    self.rcs[int(key[5:])] = np.array(h5f["key"]["run_conditions"])

    def gen(self, batch, run):
        if batch >= self.brp.number_of_batches or run >= self.brp.simulations_per_batch:
            print("Run conditions requested are not defined in file specified!")
            raise Exception
        return self.rcs[batch, run]
