
from .bao_core import *
try:
    # noinspection PyUnresolvedReferences
    import memory_profiler
except ImportError:
    print("Memory profiler not installed. Run conda install memory_profiler")
    exit(1)

__author__ = 'Frank Hellmann, hellmann@pik-potsdam.de'


# noinspection PyUnusedLocal
def run_memory_profile(result_file, brp, rhsg, rcg, observer, times, mpi_enabled=True):
    brp.number_of_batches = size * 5
    brp.simulations_per_batch = 5

    with open(os.path.join(os.path.dirname(result_file), "mem_log_rank_" + str(rank)), "w") as log:
        p_run_five_simulation = memory_profiler.profile(run_five_simulation, stream=log)
        p_run_five_simulation(result_file, brp, rhsg, rcg, observer, times)


def run_five_simulation(result_file, brp, rhsg, rcg, observer, times):
    """
    """

    if not mpi_initialized:
        print("MPI not initialized! Aborting run.")
        return

    # if am_master:
    #     print("MPI size: {}".format(size))

    pprint("MPI size: {}".format(size))

    assert isinstance(brp, BatchRunParameters)
    assert isinstance(rcg, RCGen)
    assert isinstance(rhsg, TSGen)
    assert isinstance(observer, Observer)

    batch_files = dict()
    undone_batches = list()

    if am_master:
        for batch in range(brp.number_of_batches):
            if os.path.exists(os.path.join(observer.batch_dir, str(batch) + ".hdf")):
                batch_files["batch" + str(batch)] = os.path.join(observer.batch_dir, str(batch) + ".hdf")
            else:
                undone_batches.append(batch)

        if undone_batches != list() and not os.path.exists(observer.batch_dir):
            os.mkdir(observer.batch_dir)

    undone_batches = comm.bcast(undone_batches, root=0)

    local_batch_files = dict()

    comm.Barrier()

    # Hand unrolled loops, in order to see memory leaks between iterations.

    # set index, the assignment of batches to processes differs from the normal function.

    # next batch:
    index = 0 + 5 * rank
    batch = undone_batches[index]

    observer.reset(batch)

    # Begin simulation loop:

    sim = 0

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 1

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 2

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 3

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 4

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    # end simulation loop

    batch_name = observer.result()
    print("Rank {0} finished batch {1}, saved in {2}".format(rank, batch, batch_name))
    local_batch_files["batch" + str(batch)] = batch_name

    # next batch:
    index = 1 + 5 * rank
    batch = undone_batches[index]

    observer.reset(batch)

    # Begin simulation loop:

    sim = 0

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 1

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 2

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 3

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 4

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    # end simulation loop

    batch_name = observer.result()
    print("Rank {0} finished batch {1}, saved in {2}".format(rank, batch, batch_name))
    local_batch_files["batch" + str(batch)] = batch_name

    # next batch:

    index = 2 + 5 * rank
    batch = undone_batches[index]

    observer.reset(batch)

    # Begin simulation loop:

    sim = 0

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 1

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 2

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 3

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 4

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    # end simulation loop

    batch_name = observer.result()
    print("Rank {0} finished batch {1}, saved in {2}".format(rank, batch, batch_name))
    local_batch_files["batch" + str(batch)] = batch_name
    # next batch:
    index = 3 + 5 * rank
    batch = undone_batches[index]

    observer.reset(batch)

    # Begin simulation loop:

    sim = 0

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 1

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 2

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 3

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 4

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    # end simulation loop

    batch_name = observer.result()
    print("Rank {0} finished batch {1}, saved in {2}".format(rank, batch, batch_name))
    local_batch_files["batch" + str(batch)] = batch_name

    # next batch:

    index = 4 + 5 * rank
    batch = undone_batches[index]

    observer.reset(batch)

    # Begin simulation loop:

    sim = 0

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 1

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 2

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 3

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    sim = 4

    rc = rcg.gen(batch, sim)
    observer.update(rhsg.create_ts(rc, times), sim, rc)

    # end simulation loop

    batch_name = observer.result()
    print("Rank {0} finished batch {1}, saved in {2}".format(rank, batch, batch_name))
    local_batch_files["batch" + str(batch)] = batch_name

    # end batch loop

    # Wait until all processes have finished calculating their batches
    comm.Barrier()

    if not am_master:
        comm.send(local_batch_files, dest=0, tag=rank)

    # Final processing on the master
    if am_master:
        # We update the list of batch files we are using
        batch_files.update(local_batch_files)

        for r in range(1, size):
            rec_batch_files = comm.recv(source=r, tag=r)
            batch_files.update(rec_batch_files)

        if not len(batch_files) == brp.number_of_batches:
            sys.exit("Not enough batch files present!")

        print("Collecting data.")

        with h5py.File(result_file, mode='w') as h5f:

            brp_group = h5f.create_group("batch_run_parameters")
            save_class_to_hdf5_group(brp, brp_group)

            h5f["times"] = times

            for batch_group_name, batch_file_name in batch_files.items():
                batch_group = h5f.create_group(batch_group_name)
                with h5py.File(batch_file_name, 'r') as h5bf:
                    save_dict_to_hdf5_group(load_dict_from_hdf5_group(h5bf["results"]), batch_group)

        shutil.rmtree(observer.batch_dir)

    # Wait until master is done before exiting to make sure the result file exists for all running processes.
    comm.Barrier()