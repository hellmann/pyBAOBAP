

try:
    from mpi4py import MPI
    comm = MPI.COMM_WORLD

    size = comm.size
    rank = comm.rank
    am_master = (rank == 0)
    mpi_initialized = True

except Exception as e:
    print("MPI initialization failed! Exception: {}".format(str(e)))

    class comm_shim(object):
        def Barrier(self):
            pass

        def Abort(self):
            raise Exception("MPI Abort called")

        def bcast(self, x, **kwargs):
            return x

        def Bcast(self, x, **kwargs):
            return x

        def __getattribute__(self,name):
            ret = None
            try:
                ret = object.__getattribute__(self, name)
            except:
                raise NotImplementedError(name + " is not implemented in the comm shim (pyBAOBAP mpi_tools.py)")
            return ret

    comm = comm_shim()

    mpi_initialized = False

    size = 1
    rank = 0
    am_master = (rank == 0)


# In order to make sure that in a nested call of run_on_master decorated functions only the outer layer is broadcast to
# all nodes we need a global flag.
_running_on_master_only = False


def run_on_master(f):
    if mpi_initialized:
        def f_run_once(*args, **kwargs):
            global _running_on_master_only

            # If we are not being called from a run_on_master decorated function, we will broadcast the the result at
            # the end of the function call. We also set the running on master only flag to true to notify any functions
            # called by f that are also decorated to not broadcast their result. This is necessary because such
            # functions called from here are not called on non-master processes. Attempting to broadcast such inner
            # results would lead to comm.bcast calls that are not called on all processes with hard to debug bugs.
            broadcast = not _running_on_master_only
            _running_on_master_only = True

            res = None

            if am_master:
                res = f(*args, **kwargs)

            if broadcast:
                res = comm.bcast(res, root=0)
                _running_on_master_only = False

            return res
    else:
        f_run_once = f

    return f_run_once


pprint = run_on_master(print)
