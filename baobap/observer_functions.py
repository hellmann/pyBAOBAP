
__author__ = 'Frank Hellmann, hellmann@pik-potsdam.de'
import numpy as np
from numba import njit

def run_condition_observer(states, rc):
    res = {"initial_conditions": rc[0]}
    if not rc[1] is None:
        if isinstance(rc[1], (tuple, list)):
            for i, r in enumerate(rc[1]):
                res["rhs_arguments_{}".format(i)] = r
        else:
            res["rhs_arguments"] = rc[1]
    return res


def converged(states, fp, epsilon):
    return True if np.linalg.norm(states[-1] - fp) < epsilon else False


def define_return_to_attractor_observer(attractor_list, attractor_name_list=None, epsilon=0.1):
    if attractor_name_list is None:
        attractor_name_list=list()
        for i in range(len(attractor_list)):
            attractor_name_list.append("attractor_{}".format(i))

    def return_to_attractor_obs(states, rc):
        res_dict = dict()
        for fp, fp_name in zip(attractor_list, attractor_name_list):
            res_dict[fp_name + "_converged"] = converged(states, fp, epsilon)
        return res_dict

    return return_to_attractor_obs


def define_ic_distance_to_attractor_observer(attractor_list, attractor_name_list=None):
    if attractor_name_list is None:
        attractor_name_list=list()
        for i in range(len(attractor_list)):
            attractor_name_list.append("attractor_{}".format(i))

    def start_from_attractor_obs(states, rc):
        res_dict = dict()
        for fp, fp_name in zip(attractor_list, attractor_name_list):
            res_dict[fp_name + "_dist"] = np.linalg.norm(states[0] - fp)
        return res_dict

    return start_from_attractor_obs


def quad_entropy(time_series):
    import scipy.stats as sts
    if len(time_series.shape) > 1:
        length = time_series.shape[1]
        ent = np.zeros(length, dtype=np.float64)
        for l in range(length):
            hist, bins = np.histogram(time_series[:, l])
            ent[l] = sts.entropy(hist, bins[1:] - bins[:-1])
    else:
        hist, bins = np.histogram(time_series)
        ent = sts.entropy(hist, bins[1:] - bins[:-1])
    return ent


def tail_characteristics(states, rc):
    tail_length = len(states) // 2
    tail = states[-tail_length:]
    tail_mean = np.mean(tail, axis=0)
    tail_entropy = quad_entropy(states)
    return {"tail_average": tail_mean,
            "tail_variance": np.var(tail, axis=0),
            "tail_entropy": tail_entropy
            }



def rel_entropy_to_gauss(time_series, mean, var, bino_hist):
    # The assumption is that bino_hist gives a binomial distribution with p = 0.5 and n = len(bino_hist) - 1
    # then bins are prepared to make the binomial histogram into the histogram of a binomial process with shifted mean
    # and variance to match the time_series.

    from scipy.stats import entropy
    n_binom = len(bino_hist)
    start = mean - np.sqrt(n_binom * var)
    stop = mean + np.sqrt(n_binom * var)
    if len(time_series.shape) > 1:
        length = time_series.shape[1]
        ent = np.zeros(length, dtype=np.float64)
        for l in range(length):
            bins = np.linspace(start[l], stop[l], n_binom+1)
            # we bin the time series such that if it were gaussian, it would exactly resemble the binomial histogram
            hist, _ = np.histogram(time_series[:, l], bins=bins)
            # Whatever is outside of the extreme bin boundaries is added to the first and last bin. np.sum on bool
            # arrays counts the number of Trues.
            hist[0] += np.sum(time_series[:, l] < start[l])
            hist[-1] += np.sum(time_series[:, l] > start[l])
            ent[l] = entropy(hist, bino_hist)
    else:
        bins = np.linspace(start, stop, n_binom+1)
        hist, _ = np.histogram(time_series, bins=bins)
        ent = entropy(hist, bino_hist)

    return ent


def define_asymptotic_classifier_ob(state_filter = None, tail_frac=0.5, n_binom=100):
    from scipy.stats import binom
    bn = binom(n_binom-1, 0.5)
    bino_hist = np.array([bn.pmf(k) for k in range(n_binom)])

    # if tail_quality:
    #
    #     def asymptotic_classifier_ob(states, rc):
    #         tail_len = max(int(tail_frac * len(states)), 1)
    #
    #         if state_filter is None:
    #             tail = states[tail_len:]
    #         else:
    #             tail = states[tail_len:, state_filter]
    #         tail_mean = np.mean(tail, axis=0)
    #         tail_var = np.var(tail, axis=0)
    #
    #         # make a tail that's slightly shorter to test whether the system is really converged already.
    #         tail_len_test = max(int((0.2 + 0.8 * tail_frac) * len(states)), 1)
    #
    #         if state_filter is None:
    #             tail_test = states[tail_len_test:]
    #         else:
    #             tail_test = states[tail_len_test:, state_filter]
    #
    #         tail_test_mean = np.mean(tail_test, axis=0)
    #         tail_test_var = np.var(tail_test, axis=0)
    #
    #         tail_converged_quality = np.sum((tail_mean - tail_test_mean)**2) + np.sum((tail_var - tail_test_var)**2)
    #
    #         tail_mean = np.mean(tail, axis=0)
    #         tail_var = np.var(tail, axis=0)
    #
    #         if tail_var < 0.01:
    #             tail_var = 0.
    #             tail_ent = 0.
    #         else:
    #             tail_ent = rel_entropy_to_gauss(tail, tail_mean, tail_var, bino_hist)
    #
    #         return{"mean": tail_mean,
    #                "var": tail_var,
    #                "ent": tail_ent}
    #
    # else:

    def asymptotic_classifier_ob(states, rc):
        tail_len = max(int(tail_frac * len(states)), 1)

        if state_filter is None:
            tail = states[tail_len:]
        else:
            tail = states[tail_len:, state_filter]
        tail_mean = np.mean(tail, axis=0)
        tail_var = np.var(tail, axis=0)

        if tail_var < 0.01:
            tail_var = 0.
            tail_ent = 0.
        else:
            tail_ent = rel_entropy_to_gauss(tail, tail_mean, tail_var, bino_hist)

        return{"mean": tail_mean,
               "var": tail_var,
               "ent": tail_ent}


    return asymptotic_classifier_ob


def obs_states(states, rc):
    return{"states": states}


def define_max_states_obs(filter):
    def obs_max_states(states, rc):
        return{"max_states": np.max(states[:, filter], axis=1)}

    return obs_max_states


def obs_tail(states, rc):
    return{"tail": states[-1 * len(states)//2:]}


def combine_obs(*list_of_obs):
    loo = [x for x in list_of_obs if x is not None]

    if len(loo) == 1:
        return loo[0]

    def obs(*args):
        res = dict()
        for ob in loo:
            res.update(ob(*args))
        return res

    return obs


def combine_wit(*list_of_wits):

    low = [x for x in list_of_wits if x is not None]

    if len(low) == 1:
        return low[0]

    def combi_wit(*args):
        for witt in low:
            witt(*args)

    return combi_wit


def define_figure_wittness(folder, sparsity=10, indices_to_plot=None):
    import matplotlib.pyplot as plt
    import os
    import gc

    if indices_to_plot is not None:
        def draw_figures(brp, batch, run, batch_folder, states, rc):
            if not run % sparsity:
                plt.clf()
                plt.plot(states[:, indices_to_plot])
                filename = 'batch_{}_run_{}.png'.format(batch, run)
                plt.savefig(os.path.join(folder, filename))
                plt.clf()
                plt.close("all")
                gc.collect()
            return None
    else:
        def draw_figures(brp, batch, run, batch_folder, states, rc):
            if not run % sparsity:
                plt.clf()
                plt.plot(states)
                filename = 'batch_{}_run_{}.png'.format(batch, run)
                plt.savefig(os.path.join(folder, filename))
                plt.clf()
                plt.close("all")
                gc.collect()
            return None

    return draw_figures

