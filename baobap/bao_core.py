

import os
import shutil
import sys

from scipy.integrate import odeint

from .hdfutils import *
from .mpi_tools import *
from .utility_functions import *
from .observer_functions import *
import collections

__author__ = 'Frank Hellmann, hellmann@pik-potsdam.de'


class BatchRunParameters(object):
    """" The basic parameters of the simulation:
    How many batches will we have, and how many runs will we do per batch. """

    def __init__(self, number_of_batches=1, simulations_per_batch=1, system_dimension=1):
        self.number_of_batches = number_of_batches
        self.simulations_per_batch = simulations_per_batch
        self.system_dimension = system_dimension

    @property
    def shape(self):
        return (self.number_of_batches, self.simulations_per_batch, self.system_dimension)


class TSGen(object):
    """ This class performs the actual simulation. """

    def __init__(self, brp, rhs=None, times=None, **kwargs):
        self.dim = brp.system_dimension
        self.times = times
        if rhs is not None:
            self.rhs = rhs

    def rhs(self, y, t):
        return np.zeros(self.dim)

    def create_ts(self, rc):
        """ Creates a time series at times t using the run conditions rc. """
        return odeint(self.rhs, rc[0], self.times, args=rc[1])


# noinspection PyUnusedLocal
class RCGen(object):
    """ This class generates the run condition ensemble for the batch run. """

    def __init__(self, brp, rcg=None, **kwargs):
        self.number_of_batches = brp.number_of_batches
        self.simulations_per_batch = brp.simulations_per_batch
        self.dim = brp.system_dimension
        if rcg is None:
            self._rcg = lambda batch, run: (np.zeros(self.dim), None)
        else:
            self._rcg = rcg

    def gen(self, batch, run):
        return self._rcg(batch, run)



class Observer(object):
    """
        The observer class that aggregates information per batch into the file batch_dir/batch.hdf.
    """

    # noinspection PyUnusedLocal
    def __init__(self, brp, batch_dir, verbose=False, ob=None, wittness=None):
        self.batch = 0
        self.brp = brp
        self.batch_is_set = False
        self.run_observed = [False]*brp.simulations_per_batch
        self.simulations_per_batch = brp.simulations_per_batch
        self.wittness = wittness
        if ob is None:
            self._ob = self.rc_obs
        else:
            self._ob = ob
        self.verbose = verbose
        self.batch_dir = batch_dir
        # The following list determines which variables should be hidden from the results dictionary.
        self.do_not_save = ["do_not_save", "verbose", "batch_dir", "h5_filename", "batch_is_set", "_ob", "observations",
                            "run_observed", "simulations_per_batch", "brp", "wittness"]
        self.h5_filename = ""


    def rc_obs(self, states, run, rc):
        res = {"initial_conditions": rc[0]}
        if not rc[1] is None:
            if isinstance(rc[1], (tuple, list)):
                for i, r in enumerate(rc[1]):
                    res["rhs_arguments_{}".format(i)] = r
            else:
                res["rhs_arguments"] = rc[1]
        return res


    def reset(self, batch):
        self.batch = batch
        self.h5_filename = os.path.join(self.batch_dir, str(self.batch) + ".hdf")
        self.run_observed = np.logical_xor(self.run_observed, self.run_observed)
        self.batch_is_set = True

    # noinspection PyUnusedLocal
    def update(self, time_series, run, run_conditions):
        if not self.wittness is None:
            self.wittness(self.brp, self.batch, run, self.batch_dir, time_series, run_conditions)
        self.run_observed[run] = True
        if not self.batch_is_set:
            Exception("Updating Observer that has not yet been reset!")
        if self.verbose:
            print(str(self.batch) + " " + str(run))
        obs_dict = self._ob(time_series, run_conditions)
        for key in obs_dict.keys():
            if key not in vars(self):
                data = np.array(obs_dict[key])
                setattr(self, key, np.empty((self.simulations_per_batch, ) + data.shape, dtype=data.dtype))

            field = getattr(self, key, None)
            field[run] = np.array(obs_dict[key])

    def result(self):
        """
        Saves the results that have been observed and returns the file name where the results were saved.
        """
        if not np.all(self.run_observed):
            print("Result summary called before all runs have been observed!")
        with h5py.File(self.h5_filename, 'w') as h5f:
            res = h5f.create_group("results")

            for attr in vars(self):
                if attr not in self.do_not_save:
                    data = np.array(getattr(self, attr))
                    if data.dtype == np.dtype('O'):
                        print(attr + " could not be saved")
                    else:
                        res[attr] = np.array(getattr(self, attr))

        self.batch_is_set = False

        return self.h5_filename


def validate_functions(brp, rhs, rcg, ob, times):
    try:
        t = 0.
        # batch and run parametrize the individual simulations in our experiment
        batch, run = 0, 0
        # rcg generates the initial conditions and other function arguments for each individual simulation
        ic, args = rcg(batch, run)
        assert len(ic) == brp.system_dimension
        # Now we try to call the right hand side with the data provided by rcg
        rhs(ic, t, *args)
        # Finally we try to integrate rhs for a time to generate a time series
        states = odeint(rhs, ic, times, args=args)
        # and use the function ob to generate a dictionary of observations on that simulation
        obs = ob(states, 0, (ic, args))
        if not type(obs) is dict:
            print("Validation failed: Observer function is not returning a dictionary.")
        # test that the observations returned can be composed in a list and converted to a numpy array. This is required
        # for saving them in hdf files
        for name in obs.keys():
            l = list()
            l.append(obs[name])
            l.append(obs[name])
            assert np.array(l).dtype != np.dtype('O')
    except Exception as e:
        print("Could not validate the set of functions.\n", e)
        return False

    return True

#
# def resume_experiment(resume_file):
#     import cPickle as pickle
#     with open(resume_file, 'r') as f:
#         args = pickle.load(f)
#     run_experiment(*args)
#
#     return args[0]


def run_experiment(result_file, brp, ts_gen, rc, ob, times=None, batch_dir=None, verbose=True, figures=None,
                   wittness=None, rhs=None):
    #
    # if am_master:
    #     import cPickle as pickle
    #     resume_file = os.path.join(os.path.dirname(result_file), "resume.p")
    #     with open(resume_file, 'w') as f:
    #         pickle.dump((result_file, brp, ts_gen, rc, ob, times, batch_dir, verbose, figures, wittness, rhs), f)


    if batch_dir is None:
        batch_dir = os.path.join(os.path.split(result_file)[0], "batch_data")
        ensure_dir_exists(batch_dir)
        comm.Barrier()

    if isinstance(ts_gen, collections.Callable):
        # TODO the way that the times parameter is passed around needs to absolutely be reworked!!!
        tsg = TSGen(brp, times=times)
        tsg.create_ts = ts_gen
    elif isinstance(ts_gen, TSGen):
        tsg = ts_gen
    elif isinstance(rhs, collections.Callable):
        if times is None:
            print("No times to create timeseries for given!")
            return
        tsg = TSGen(brp, rhs=rhs, times=times)
    else:
        print("No valid right hand side function or RhsGen class given.")
        return

    if isinstance(rc, RCGen):
        rcg = rc
    elif isinstance(rc, collections.Callable):
        rcg = RCGen(brp, rcg=rc)
    else:
        print("No valid run condition generator function or RCGen class given.")
        return

    # Depending on the figures parameter we initialize the figures wittness. This will have no effect if an Observer
    # class is provided, but if a callable observer function is provided this option initializes an observer that
    # records figures of the simulations.

    fig_wittness = None
    if isinstance(figures, (tuple, list)):
        if figures[0] is True:
            fig_dir = os.path.join(os.path.dirname(result_file), "figures")
            ensure_dir_exists(fig_dir)
            fig_args = (fig_dir,) + figures[1:]
        else:
            fig_args = figures
        from .observer_functions import define_figure_wittness
        fig_wittness = define_figure_wittness(*fig_args)
    elif figures is True:
        fig_dir = os.path.join(os.path.dirname(result_file), "figures")
        ensure_dir_exists(fig_dir)
        from .observer_functions import define_figure_wittness
        fig_wittness = define_figure_wittness(fig_dir)

    c_wittness = combine_wit(wittness, fig_wittness)

    if isinstance(ob, Observer):
        obs = ob
    elif isinstance(ob, collections.Callable):
        obs = Observer(brp, batch_dir, ob=ob, wittness=c_wittness)
    else:
        print("No valid oberver function or Observer class given.")
        return

    run_simulation_classes(result_file, brp, tsg, rcg, obs, verbose=verbose)
    #
    # if am_master:
    #     os.remove(resume_file)

def run_simulation_functions(result_file, brp, rhs, rcg, ob, times, batch_dir=None, validate=True, verbose=True):
    assert isinstance(brp, BatchRunParameters)
    # Validate the functions passed, to see if they have the correct semantics for batch runs and observations.
    if validate:
        if not validate_functions(brp, rhs, rcg, ob, times):
            return

    if batch_dir is None:
        batch_dir = os.path.join(os.path.split(result_file)[0], "batch_data")
        if am_master:
            if not os.path.exists(batch_dir):
                os.mkdir(batch_dir)
        comm.Barrier()

    rhsg = TSGen(brp, rhs=rhs, times=times)
    rcg = RCGen(brp, rcg=rcg)
    observer = Observer(brp, batch_dir, ob=ob)
    run_simulation_classes(result_file, brp, rhsg, rcg, observer, verbose=verbose)


def run_simulation_classes(result_file, brp, rhsg, rcg, observer, verbose=True):
    if verbose:
        pprint("MPI size: {}".format(size))

    assert isinstance(brp, BatchRunParameters)
    assert isinstance(rcg, RCGen)
    assert isinstance(rhsg, TSGen)
    assert isinstance(observer, Observer)

    batch_files = dict()
    undone_batches = list()

    if am_master:
        for batch in range(brp.number_of_batches):
            if os.path.exists(os.path.join(observer.batch_dir, str(batch) + ".hdf")):
                batch_files["batch" + str(batch)] = os.path.join(observer.batch_dir, str(batch) + ".hdf")
            else:
                undone_batches.append(batch)

        if undone_batches != list() and not os.path.exists(observer.batch_dir):
            os.mkdir(observer.batch_dir)

    undone_batches = comm.bcast(undone_batches, root=0)

    local_batch_files = dict()

    comm.Barrier()

    for index, batch in enumerate(undone_batches):
        if (index - rank) % size == 0:
            observer.reset(batch)

            for sim in range(brp.simulations_per_batch):
                rc = rcg.gen(batch, sim)
                observer.update(rhsg.create_ts(rc), sim, rc)

            batch_name = observer.result()
            if verbose:
                print("Rank {0} finished batch {1}, saved in {2}".format(rank, batch, batch_name))
            local_batch_files["batch" + str(batch)] = batch_name

    # Wait until all processes have finished calculating their batches
    comm.Barrier()

    if not am_master:
        comm.send(local_batch_files, dest=0, tag=rank)

    # Final processing on the master
    if am_master:
        # We update the list of batch files we are using
        batch_files.update(local_batch_files)

        for r in range(1, size):
            rec_batch_files = comm.recv(source=r, tag=r)
            batch_files.update(rec_batch_files)

        if not len(batch_files) == brp.number_of_batches:
            sys.exit("Not enough batch files present!")

        if verbose:
            print("Collecting data.")

        with h5py.File(result_file, mode='w') as h5f:

            brp_group = h5f.create_group("batch_run_parameters")
            save_class_to_hdf5_group(brp, brp_group)

            h5f["times"] = rhsg.times if rhsg.times is not None else "None"

            for batch_group_name, batch_file_name in batch_files.items():
                batch_group = h5f.create_group(batch_group_name)
                with h5py.File(batch_file_name, 'r') as h5bf:
                    save_dict_to_hdf5_group(load_dict_from_hdf5_group(h5bf["results"]), batch_group)

        shutil.rmtree(observer.batch_dir)

    # Wait until master is done before exiting to make sure the result file exists for all running processes.
    comm.Barrier()
